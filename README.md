# Deploy Camunda 7 with Score

#### Deploy App With Score

Score being a universal specification, the CLI you use determines to which target infrastructure type your application is going to deployed to. When writing this tutorial, the officialy supported options are
- docker-compose
- helm
- humanitec

Since we want to deploy to an environment managed by Humanitec, we are going to choose `score-humanitec`. Deploy with following command assuming that above score file is called `score.yaml`

```sh
./score-humanitec delta --org $HUMANITEC_ORG  --app $MY_APP_NAME --env development --token $HUMANITEC_TOKEN --file score.yaml --deploy
```

The parameters being:
- `--org HUMANITEC_ORG` : id of your Humanitec organzation your Application is located in
- `--app $MY_APP` : the name Humanitec should show in UI for this application
- `--token $HUMANITEC_TOKEN` : providing the actual API token
- `--file score.yaml` : path to the score file
- `--deploy` : deploy the uploaded delta immediately

## Troubleshooting

- Anticipate common mistakes or problems and explain how to solve or avoid them.
- If there are error messages, explain what they mean and what to do about them.


## Recap

Summarize what the user should have learned and achieved.

## Next Steps

Suggest related features or tasks the user might want to learn about next. Link to related tutorials if applicable.

## Additional Resources

- [Humanitec Developers Documentation](https://developer.humanitec.com)

## Feedback and Contact

If you have feedback, raise an issue at this project.

## License
EUPL 1.2

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
